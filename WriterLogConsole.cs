﻿using System;
using System.Collections.Generic;
using System.IO;

namespace WriterLogConsole
{
    public class WriterLogConsole
    {
        private static Dictionary<string, bool> check = new Dictionary<string, bool>();

        public enum Action
        {
            CONTINUE,
            EXIT
        };

        public enum Log
        {
            INFO,
            TODO,
            ERROR,
            DONE,
            END_ERROR,
            END_OK,
            RETRIEVE
        };

        public void Progress(int i, int tot)
        {
            Console.ResetColor();

            int perc = (i * 100) / tot;

            if (0 <= perc && perc <= 34)
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }
            else
            if (35 <= perc && perc <= 74)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
            }
            else
            if (75 <= perc && perc <= 99)
            {
                Console.ForegroundColor = ConsoleColor.Green;
            }

            Console.Write(i + "/" + tot + " (" + perc + "%)");

            Console.SetCursorPosition(0, Console.CursorTop);
        }

        public void Writer(Log l, string msg, string path = null)
        {
            string prefix = string.Empty;

            switch (l)
            {
                case Log.INFO:
                    prefix = "INFO => ";
                    break;
                case Log.TODO:
                    prefix = "TODO => ";
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case Log.ERROR:
                    prefix = "ERROR =< ";
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case Log.END_ERROR:
                    prefix = "END ERROR =< ";
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case Log.DONE:
                    prefix = "DONE =< ";
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case Log.END_OK:
                    prefix = "END OK =< ";
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
            }

            string toprint = "[" + DateTime.Now + "]" + prefix + msg;

            if (l != Log.RETRIEVE)
            {
                Console.WriteLine(toprint);
            }

            if (path != null && (!string.IsNullOrWhiteSpace(path) || !string.IsNullOrEmpty(path)))
            {
                bool success = false;

                try
                {
                    if (!check.TryGetValue(path, out success))
                    {
                        var pathbefore = CheckDir(path);

                        if (!Directory.Exists(pathbefore))
                        {
                            Directory.CreateDirectory(pathbefore);
                        }

                        check.Add(path, true);
                    }

                    if (check[path])
                    {
                        using (StreamWriter sw = File.AppendText(path))
                        {
                            sw.WriteLine(((l != Log.RETRIEVE) ? toprint : msg));
                        }
                    }
                    else
                    {
                        Writer(Log.ERROR, "Path non corretto, scrittura non eseguita");
                    }
                }
                catch (Exception ex)
                {
                    if (check.TryGetValue(path, out success))
                    {
                        check[path] = false;
                    }
                    else
                    {
                        check.Add(path, false);
                    }

                    Writer(Log.ERROR, ex.Message + " / " + ex.InnerException);
                }

            }

            if (l != Log.INFO)
            {
                Console.ResetColor();
            }
        }
        private string CheckDir(string path)
        {
            var pathbefore = path.Split('.')[0];

            int index = (pathbefore.Length) - 1;

            for (int pos = index; pos > 0; pos--)
            {
                if (!pathbefore[pos].Equals('\\'))
                {
                    pathbefore = pathbefore.Remove(pos);
                }
                else
                {
                    pathbefore = pathbefore.Remove(pos);
                    break;
                }
            }

            return pathbefore;
        }
        public void BreakPoint(Action a)
        {
            string log = string.Empty;
            switch (a)
            {
                case Action.CONTINUE:
                    log = "continue";
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;

                case Action.EXIT:
                    log = "exit";
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
            }

            Console.WriteLine(string.Format("[{1}]Press ENTER to {0}...", log, DateTime.Now));
            Console.ResetColor();
            Console.ReadLine();
        }
    }
}

